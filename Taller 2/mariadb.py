from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
import random 

engine = create_engine('mysql://root:javier25*@localhost/diccionario')
Base =declarative_base()

Session = sessionmaker(bind=engine)
session = Session()

class slang(Base):
    __tablename__ = 'diccionario'
    codigo=Column(Integer, primary_key=True,default=random.randint(0,999))
    palabra = Column(String,primary_key=True, default='tarea por defecto')
    definicion = Column(String, default='tarea por defecto')
    def __init__(self,palabra,definicion):
        self.palabra = palabra
        self.definicion = definicion

def __repr__(self):
 return self.opciones

 Base.metadata.create,all(engine)

def slang_panameno():
    palabra = input("Digite la palabra del slang panameño: ")
    definicion = input("Digite la definicion de la palabra: ")
    session.add(slang(palabra,definicion))
    session.commit()
    opciones()

def editar_palabra():
    palabra = input("Escriba la nueva palabra: ")
    definicion = input("Escriba la nueva definicion: ")
    try:
        res = session.query(slang).filter(slang.palabra == palabra).one()
        res.definicion = definicion
        session.add(res)
        session.commit()
        print("Palabra editada :) ")
    except:
        print("no se pudo completar la accion")
    input()
    opciones()

def eliminar_palabra():
    #para obtener todo:
    resultado = session.query(slang).all()
    print('Mi tabla de slang')
    for i in resultado:
        print('codigo:'+str(i.codigo)+' palabra:'+i.palabra+' definicion:'+i.definicion)
    codigo = input("Ingrese el codigo de la palabra a borrar: ")
    try:
        res = session.query(slang).filter(slang.codigo == codigo).one()
        session.delete(res)
        session.commit()
        print("Palabra borrada")
    except:
        print("no se pudo completar accion")
    input()
    opciones()

def ver_palabras():
    print("\t Palabras agregadas: ")
    print("\t palabra ")
    print("___________________________________")
    resultado = session.query(slang).all()
    print('Mi tabla de slang')
    for i in resultado:
        print('palabra:'+i.palabra)
    print("\n\n")
    opciones()

def buscar_significado():
    palabra = input("Escriba la palabra ")
    try:
        res = session.query(slang).filter(slang.palabra == palabra).one()
        print("Palabra:"+res.palabra+" Definicion:"+res.definicion)
    except:
        print('no econtrada')
    input()
    opciones()

def opciones():
    print("1- Agregar nueva palabra")
    print("2- Editar palabra existente")
    print("3- Eliminar palabra existente")
    print("4- Ver listado de palabras")
    print("5- Buscar significado de palabras")
    print("6- Salir")
    opcion=input("Seleccione una opcion:")
    if opcion=='1':
        slang_panameno()
    elif opcion == '2':
        editar_palabra()
    elif opcion=='3':
        eliminar_palabra()
    elif opcion=='4':
        ver_palabras()
    elif opcion=='5':
         buscar_significado()
    elif opcion=='6':
        print("Regrese pronto:")
        input()
    else:
        input("Seleccione una opcion valida !!!")
opciones()
